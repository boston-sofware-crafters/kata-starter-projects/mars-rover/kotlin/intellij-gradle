Let's take a trip to Mars. I'm Paul Reilly, flight director. We'll use my screen to show your flight instructions. I'll be taking this trip with you.

These instructions are included in your facilitator's IDE. Refer back to them as necessary during the trip.

We are in a NASA Mars Rover headed for the Meridian Plateau, a ten mile by ten mile grid we have been assigned to explore.

We will land at coordinates zero zero -- using x, y coordinates --- facing North. This is the bottom left cell on the grid seen as we land.

Our rover can do two things very well:
1. First, we can turn left or right to face a different direction. These are commands L and R. Think L as counter clockwise, R as clockwise.
2. Second, we can move one mile in the current direction. This is the M command.

Our rover also is fitted with a quantum wrap drive such that if we are about to move off the grid, instead we wrap around in the current direction.

From the starting position, an M command takes us to coordinate zero one still facing North.

Nine more move commands will take us back to coordinate zero zero, still facing North (because of the quantum wrap drive).

An L command will have us turn to face West if we are currently facing North.

Another L command has us face South, another East and another and we've done a complete circuit.

Similarly an R command when facing North has us facing East. One more and we face South, then West and then back North.

While we are on Mars, NASA will beam us to a new plateau where we will be at coordinate (0, 0) facing North. They will then ask us to execute some commands.

The commands will be packed in a string, for example, el em ar.

When we finish executing the commands we must report our current coordinates and direction in a coded format, for example seven colon three colon ee.

When sending commands, NASA may also send us a list of coordinates where obstacles are located. By default, there will be no obstacles.

We cannot move past these obstacles and must report our current position and direction when one is encountered, prefixed with an "O", for example O : 0 : 5 : w.

Your job, while we are in flight, is to program the on-board computer, using the language you chose, to process the commands and return the results back to NASA.

Ensure that your program will provide a method named execute() which takes a command and a (possibly empty) list of obstacle coordinates.

The method will return the current position after executing the command.

What we do know is the Meridian Plateau has no obstacles. A command sequence of double M R double M L M puts us at coordinate 2 3 facing North so return 2 : 3 : N.

At some point NASA will beam us to the Solis Plateau. This grid has an obstacle at coordinates zero four.

We are asked to execute the command R plus 5 ems so we must send back oh : zero : three : ee, where the leading oh indicates we have encountered an obstacle. Be careful! Don't get your O's and 0's confused.

We also know that the command consisting of 6 ems, an R, 8 more ems, an L, an M, an R and 3 ems executed after we arrive on Meridian will wrap and we should return 1 : 7 : E.

While developing your program, use the standard TDD approach with your crew members. You will take turns navigating and driving.

Also, please ensure that every line of your program is covered by a test using the available code coverage tool.

Here's the two things you need need to know about a code coverage report:

1. First, the report tells you where you need to add tests: any statement with a red background needs a test,
   any statement with a yellow background is missing a branch test.
2. Second, the report tells you when you are done testing: all tests pass and all statements have a green background.

Now you can retire to your assigned sessions to develop your program.  We will arrive at 20:30. Please be ready.

Any questions before you go? ... put them in the chat window please.

Enjoy your flight!

If you will be writing a Kotlin program stay in this Zoom session.
 
Otherwise leave this session (the leave control should be on the bottom right) and rejoin this session at twenty thirty precisely.

See the assignments in Slack for a link to your facilitator's session.

